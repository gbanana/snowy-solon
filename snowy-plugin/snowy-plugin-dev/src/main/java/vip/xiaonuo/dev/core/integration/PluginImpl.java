package vip.xiaonuo.dev.core.integration;

import org.noear.solon.core.AopContext;
import org.noear.solon.core.Plugin;
import vip.xiaonuo.common.annotation.CommonLog;
import vip.xiaonuo.dev.core.aop.DevLogInterceptor;

/**
 * @author noear 2022/10/21 created
 */
public class PluginImpl implements Plugin {
    @Override
    public void start(AopContext context) {
        context.beanAroundAdd(CommonLog.class, new DevLogInterceptor());
    }
}
