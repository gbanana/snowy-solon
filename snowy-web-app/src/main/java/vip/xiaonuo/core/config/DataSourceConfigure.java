package vip.xiaonuo.core.config;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import org.noear.solon.data.dynamicds.DynamicDataSource;

import javax.sql.DataSource;

/**
 * @author noear 2022/10/7 created
 */
@Configuration
public class DataSourceConfigure {
    @Bean(name = "snowy", typed = true)
    public DataSource db1(@Inject("${snowy.datasource.dynamic}") DynamicDataSource ds) {
       return ds;
    }

//    @Bean
//    public void db1Init(@Db("db1")MybatisConfiguration configuration){
//        //这块，赞时不支持配置。手动处理下
//        configuration.getTypeHandlerRegistry().register("vip.xiaonuo.common.handler");
//    }
}
